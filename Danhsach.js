"use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */ 
    var gCountryObjects = [
        {
          "value": "VN",
          "text": "Việt Nam" 
        },
        {
          "value": "USA",
          "text": "USA" 
        },
        {
          "value": "AUS",
          "text": "Australia" 
        },
        {
          "value": "CAN",
          "text": "Canada" 
        }
    ];
    // Khai báo biến mảng hằng số chứa danh sách tên các thuộc tính
    const gUSER_COLS = ["id", "firstname", "lastname", "country", "subject", "customerType", "registerStatus", "action"];
    // Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
    const gUSER_ID_COL = 0;
    const gUSER_FIRSTNAME_COL = 1;
    const gUSER_LASTNAME_COL = 2;
    const gUSER_COUNTRY_COL = 3;
    const gUSER_SUBJECT_COL= 4;
    const gUSER_CUSTOMER_TYPE_COL = 5;
    const gUSER_REGISTER_STATUS_COL = 6;
    const gUSER_ACTION_COL = 7;
    // Khai báo DataTable & mapping collumns
    var gUserTable = $("#user-table").DataTable({
        columns: [
            { data: gUSER_COLS[gUSER_ID_COL] },
            { data: gUSER_COLS[gUSER_FIRSTNAME_COL] },
            { data: gUSER_COLS[gUSER_LASTNAME_COL] },
            { data: gUSER_COLS[gUSER_COUNTRY_COL] },
            { data: gUSER_COLS[gUSER_SUBJECT_COL] },
            { data: gUSER_COLS[gUSER_CUSTOMER_TYPE_COL] },
            { data: gUSER_COLS[gUSER_REGISTER_STATUS_COL] },
            { data: gUSER_COLS[gUSER_ACTION_COL] },
        ],
        columnDefs: [
            {
                // Định nghĩa lại cột Country
                targets: gUSER_COUNTRY_COL,
                render: function (data) {
                    return getCountryTextByCountryValue(data);
                }
            },
            { // Định nghĩa lại cột Action
                targets: gUSER_ACTION_COL,
                className: "text-center",
                defaultContent: `
                <button class="btn btn-primary btn-edit">Sửa</button>
                <button class="btn btn-danger btn-delete" data-toggle='modal'>Xóa</button>
                `
            }
        ],
        autoWidth: false
    });
    // Biến toàn cục để lưu trữ id user đang được update or delete. Mặc định = 0;
    var gIdOfUser = "";
    //Khai báo biến mảng chứa dữ liệu users
    var gListUsers = [];
    // Khai báo biến hằng số
    const gNOT_SELECT_COUNTRY = "NOT_SELECT_COUNTRY"; // giá trị (value) khi chưa chọn countruy
    const gNOT_SELECT_CUSTOMER_TYPE = "NOT_SELECT_CUSTOMER_TYPE"; // giá trị (value) khi chưa chọn customer type
    const gNOT_SELECT_REGISTER_STATUS ="NOT_SELECT_REGISTER_STATUS"; // giá trị (value) khi chưa chọn register status
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    $(document).ready(function(){
        onLoadPaging();
        
        // 2 - C: gán sự kiện Create - Thêm mới user
        $("#btn-add-user").on("click", function() {
            onBtnAddNewUserClick();
        });
        // 3 - U: gán sự kiện Update - Sửa 1 user
        $("#user-table").on("click", ".btn-edit", function(){
            onBtnEditClick(this);
        });
        // 4 - D: gán sự kiện Delete - xóa 1 user
        $("#user-table").on("click", ".btn-delete", function(){ 
            onIconDeleteUserClick(this);
        });

        $("#btn-create-user").on("click", function() {
            onBtnCreateUserClick();
         });
         
         $("#btn-update-user").on("click", function(){
            onBtnUpdateUserClick();
        });

        $("#btn-confirm-delete-user").on("click", function(){ 
            onBtnConfirmDeleteUserClick();
        });
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onLoadPaging(){
        "use strict"; 
        getAllUsers(); 
        // 1 - R: Read / Load user to DataTable
        loadDataToUserTable(gListUsers);
    }
    // Hàm xử lý sự kiện khi nút Thêm mới được click
    function onBtnAddNewUserClick() {
        // hiển thị modal trắng lên
        $("#create-user-modal").modal("show");
    }
    //hàm xử lý sự kiện click nút sửa
    function onBtnEditClick(paramButtonElement){
        "use strict";
        console.log("Button Edit Click!!!");
        var vRowClick = $(paramButtonElement).closest("tr"); //xác định tr chứa nút bấm đc click
        var vTable = $("#user-table").DataTable(); //tạo biến truy xuất đến datatable
        var vDataRow = vTable.row(vRowClick).data(); //lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
        gIdOfUser = vDataRow.id;
        console.log("ID = " + gIdOfUser);
        showUpdateUserModal(vDataRow);
    }
    // Hàm xử lý sự kiện khi icon delete user trên bảng được click
    function onIconDeleteUserClick(paramButtonElement){
        "use strict";
        var vRowClick = $(paramButtonElement).closest("tr"); //xác định tr chứa nút bấm đc click
        var vTable = $("#user-table").DataTable(); //tạo biến truy xuất đến datatable
        var vDataRow = vTable.row(vRowClick).data(); //lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
        // lưu thông tin id của user đang được delete vào biến toàn cục
        gIdOfUser = vDataRow.id;
        // hiển thị modal confirm delete lên
        $("#delete-user-modal").modal("show");
    }
    //hàm xử lý sự kiện click nút insert user
    function onBtnCreateUserClick(){
        "use strict";
        // khai báo đối tượng chứa user data
        var vUserObj = {
            firstname: "",
            lastname: "",
            subject: "",
            country: ""
        }
        //B1: thu thập dữ liệu
        getUserDataCreate(vUserObj);
        //B2: Validate insert
        var vIsUserValidate = validateInsertData(vUserObj);        
        if (vIsUserValidate) {
            // B3: insert user
            postUserApi(vUserObj);
            //B4: xử lý front-end 
            //gọi api lấy danh sách user 
            getAllUsers(); 
            // load lại vào bảng (table)
            loadDataToUserTable(gListUsers);
            resertCreateUserForm();
            $("#insert-user-modal").modal("hide"); //ẩn modal form create user
        } 
    }
    //hàm xử lý sự kiện click nút update user
    function onBtnUpdateUserClick(){
        "use strict";
       // khai báo đối tượng chứa user data 
        var vUserObj = {
            firstname: "",
            lastname: "",
            subject: "",
            country: "",
            customerType: "",
            registerStatus: ""
        }
        //B1: thu thập dữ liệu
        getUserDataUpdate(vUserObj);
         // B2: Validate update
        var vIsUserValidate = validateUpdateData(vUserObj);
        if (vIsUserValidate) { 
            // B3: update user
            updateUserById(vUserObj);
            // B4: xử lý front-end
            //load lại bảng
            getAllUsers(); 
            loadDataToUserTable(gListUsers);
            resetUpdateUserModalForm(); // xóa trắng modal
            $("#update-user-modal").modal("hide"); //ẩn modal form update user
        }
    }
    //hàm xử lý sự kiện click nút confirm delete user
    function onBtnConfirmDeleteUserClick(){
        "use strict";
        // B1: thu thập dữ liệu (ko có)
        // B2: validate (ko có)
        // B3: xóa user
        $.ajax({
            async: false,
            url: "http://42.115.221.44:8080/crud-api/users/" + gIdOfUser,
            type: "DELETE",
            success: function(res){
                console.log(res);
                // B4: xử lý, hiển thị trên front-end
                alert("Đã xóa người dùng thành công!");
            },
            error: function(error){
                alert(error.responseText);
            }
        }); 
        //load lại bảng
        getAllUsers();  
        loadDataToUserTable(gListUsers);
        $("#delete-user-modal").modal("hide"); //ẩn modal form delete user
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // hàm lấy text country theo value của country
    function getCountryTextByCountryValue(paramCountry){
        "use strict";
        var vTextCountry = "";
        for (var bI = 0; bI < gCountryObjects.length; bI++){
                if(gCountryObjects[bI].value == paramCountry){
                vTextCountry = gCountryObjects[bI].text;
            }
        };  
        return vTextCountry;   
    }
    //Hàm gọi API lấy danh sách user
    function getAllUsers(){
        "use strict";
        $.ajax({
            async: false,
            url: "http://42.115.221.44:8080/crud-api/users/",
            type: "GET",
            dataType: "json",
            success: function(res){
                // lấy response trả về và gán cho biến gListUsers
                gListUsers = res;
            },
            error: function(error){
                console.log(error.responseText);
            }
        }) 
    }
    /**  Load user to DataTable
    * in: user array
    * out: user table has data
    */  
    function loadDataToUserTable(paramObjectUsers) {
        console.log(paramObjectUsers);
        gUserTable.clear();
        gUserTable.rows.add(paramObjectUsers);
        gUserTable.draw();
    }
    // hàm thu thập dữ liệu để insert user
    function getUserDataCreate(paramUserObj){
        "use strict";
        paramUserObj.firstname = $("#inp-firstname").val().trim();
        paramUserObj.lastname = $("#inp-lastname").val().trim();
        paramUserObj.subject = $("#inp-subject").val().trim();
        paramUserObj.country = $("#select-country").val();
    }
    // hàm validate data khi insert
    function validateInsertData(paramUserObj){
        "use strict";
        if(paramUserObj.firstname == ""){
            alert("Firstname cần nhập");
            return false;
        };
        if(paramUserObj.lastname == ""){
            alert("Lastname cần nhập");
            return false;
        };
        if(paramUserObj.subject == ""){
            alert("Subject cần nhập");
            return false;
        };
        if(paramUserObj.country == gNOT_SELECT_COUNTRY){
            alert("Country cần chọn");
            return false;
        };
        return true;
    }
    //hàm gọi API insert user
    function postUserApi(paramUserObj){
        "use strict";
        $.ajax({
            async: false,
            url: "http://42.115.221.44:8080/crud-api/users/",
            type: "POST",
            data: JSON.stringify(paramUserObj),
            contentType: "application/json",
            success: function(res){
                console.log(res);
                alert("Thêm user thành công!");
            },
            error: function(error){
                alert(error.responseText);
            }
        });
    }
    //hàm hiện modal update cùng thông tin của user
    function showUpdateUserModal(paramDataRow){
        "use strict";
        console.log(paramDataRow);
        $("#update-user-modal").modal("show"); //hiện update modal
        $("#inp-update-firstname").val(paramDataRow.firstname);
        $("#inp-update-lastname").val(paramDataRow.lastname);
        $("#inp-update-subject").val(paramDataRow.subject);
        $("#select-update-country").val(paramDataRow.country);
        $("#select-update-customer-type").val(paramDataRow.customerType);
        $("#select-update-register-status").val(paramDataRow.registerStatus);
    }
    //hàm thu thập dữ liệu nhập trên form update user
    function getUserDataUpdate(paramUserObj){
        "use strict";
        paramUserObj.firstname = $("#inp-update-firstname").val().trim();
        paramUserObj.lastname = $("#inp-update-lastname").val().trim();
        paramUserObj.subject = $("#inp-update-subject").val().trim();
        paramUserObj.country = $("#select-update-country").val();
        paramUserObj.customerType = $("#select-update-customer-type").val();
        paramUserObj.registerStatus = $("#select-update-register-status").val();
    }
    //hàm validate dữ liệu nhập trên form update user
    function validateUpdateData(paramUserObj){
        "use strict";
        if(paramUserObj.firstname == ""){
            alert("Firstname cần nhập");
            return false;
        };
        if(paramUserObj.lastname == ""){
            alert("Lastname cần nhập");
            return false;
        };
        if(paramUserObj.subject == ""){
            alert("Subject cần nhập");
            return false;
        };
        if(paramUserObj.country == gNOT_SELECT_COUNTRY){
            alert("Country cần chọn");
            return false;
        };
        if(paramUserObj.customerType == gNOT_SELECT_CUSTOMER_TYPE){
            alert("Customer Type cần chọn");
            return false;
        };
        if(paramUserObj.registerStatus == gNOT_SELECT_REGISTER_STATUS){
            alert("Register Status cần chọn");
            return false;
        };
        return true;
    }
    // hàm thực hiện update user
    function updateUserById(paramUserObj){
        "use strict";
        $.ajax({
            async: false,
            url: "http://42.115.221.44:8080/crud-api/users/" + gIdOfUser,
            type: "PUT",
            data: JSON.stringify(paramUserObj),
            contentType: "application/json",
            success: function(res){
                console.log(res);
                alert("Đã update user thành công!");
            },
            error: function(error){
                alert(error.responseText);
            }
        });
    }
    // hàm xóa trắng form create user
    function resertCreateUserForm() {
        $("#inp-firstname").val("");
        $("#inp-lastname").val("");
        $("#inp-subject").val("");
        $("#select-country").val(gNOT_SELECT_COUNTRY);
    }
    // hàm xóa trắng form update user
    function resetUpdateUserModalForm(){
        "use strict";
        $("#inp-update-firstname").val("");
        $("#inp-update-lastname").val("");
        $("#inp-update-subject").val("");
        $("#select-update-country").val(gNOT_SELECT_COUNTRY);
        $("#select-update-customer-type").val(gNOT_SELECT_CUSTOMER_TYPE);
        $("#select-update-register-status").val(gNOT_SELECT_REGISTER_STATUS);
    }